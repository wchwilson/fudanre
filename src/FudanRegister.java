import static com.google.common.base.Preconditions.checkNotNull;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

public class FudanRegister {
	public static class VerifyException extends RuntimeException {
		private static final long serialVersionUID = 1L;
	}

	private CloseableHttpClient httpclient = HttpClients.createDefault();
	public String userId;
	private Logger logger;
	private static String password = "89ec9967312a";

	static enum MemberType {
		Full, Student, Guest
	};

	static class RegInfo {
		String paperId;
		String firstName;
		String lastName;
		String country;
		String affiliation;
		String email;
		boolean vegetarian;
		boolean muslim;
		MemberType type;

		@Override
		public String toString() {
			String r = Objects.toStringHelper(RegInfo.class).add("firstName", firstName).add("lastName", lastName)
					.add("country", country).add("affiliation", affiliation).add("email", email)
					.add("vegetarian", vegetarian).add("muslim", muslim).add("type", type).toString();
			return r;
		}

		public void check() {
			checkNotNull(firstName);
			checkNotNull(lastName);
			checkNotNull(country);
			checkNotNull(affiliation);
			checkNotNull(email);
			checkNotNull(type);
		}
	}

	public FudanRegister() {
		userId = UUID.randomUUID().toString().replaceAll("-", "");
		BigInteger uuid = new BigInteger(userId, 16);
		// .substring(8) ;
		userId = uuid.toString(36) + "@a.o";
		logger = LogManager.getLogger(userId);
		logger.info("new FudanRegister");
	}

	private static String validateUrl = "http://register.fudan.edu.cn/p/validateCode.jsp";

	public void GetV() throws ClientProtocolException, IOException {
		HttpPost httpPost = new HttpPost(validateUrl);
		CloseableHttpResponse response = httpclient.execute(httpPost);

		try {
			logger.info("GetV Status: " + response.getStatusLine());
			HttpEntity entity = response.getEntity();
			FileOutputStream fo = new FileOutputStream("test.jpeg");
			entity.writeTo(fo);
			fo.close();
		} finally {
			response.close();
		}
	}

	private String Send(String name, String url, List<NameValuePair> nvps) throws ClientProtocolException, IOException {
		HttpPost httpPost = new HttpPost(url);
		if (nvps != null) {
			httpPost.setEntity(new UrlEncodedFormEntity(nvps));
		}
		CloseableHttpResponse response = httpclient.execute(httpPost);

		try {
			StatusLine rl = response.getStatusLine();
			logger.info(name + " Status: " + rl.toString());
			if (rl.getStatusCode() != 200) {
				logger.error("response error: " + url + " " + nvps);
				throw new RuntimeException("");
			}
			HttpEntity entity = response.getEntity();
			String content = EntityUtils.toString(entity);
			// System.out.println(content);
			return content;
		} finally {
			response.close();
		}
	}

	private static String registerUrl = "http://register.fudan.edu.cn/p/en/add_check.html?dataType=onlyRecord";
	private static Pattern registerPattern = Pattern.compile("\\{records:\\{root:\"");

	private void Register(String verify, String fullName) throws ClientProtocolException, IOException {
		List<NameValuePair> nvps = Lists.newArrayList();
		nvps.add(new BasicNameValuePair("userId", userId));
		nvps.add(new BasicNameValuePair("plusInfo1", password));
		// nvps.add(new BasicNameValuePair("plusInfo2", password));
		nvps.add(new BasicNameValuePair("name", fullName));
		nvps.add(new BasicNameValuePair("verify", verify));
		String content = Send("Register", registerUrl, nvps);
		Matcher matcher = registerPattern.matcher(content);
		if (!matcher.find()) {
			throw new VerifyException();
		}
	}

	private static String loginUrl = "http://register.fudan.edu.cn/p/en/index.html";

	private void Login() throws ClientProtocolException, IOException {
		List<NameValuePair> nvps = Lists.newArrayList();
		nvps.add(new BasicNameValuePair("IDToken1", userId));
		nvps.add(new BasicNameValuePair("IDToken2", password));
		Send("Login", loginUrl, nvps);
	}

	private static String applyUrl = "http://register.fudan.edu.cn/p/en/applyModify2/5308.html";

	private void Apply(RegInfo regInfo) throws ClientProtocolException, IOException {
		List<NameValuePair> applynvps = Lists.newArrayList();
		applynvps.add(new BasicNameValuePair("projectId", "5308"));
		if (regInfo.paperId != null) {
			applynvps.add(new BasicNameValuePair("13907", regInfo.paperId));
		}
		applynvps.add(new BasicNameValuePair("13927", regInfo.firstName));
		applynvps.add(new BasicNameValuePair("13937", regInfo.lastName));
		applynvps.add(new BasicNameValuePair("13947", regInfo.country));
		applynvps.add(new BasicNameValuePair("13957", regInfo.affiliation));
		applynvps.add(new BasicNameValuePair("13967", regInfo.email));
		if (regInfo.vegetarian) {
			applynvps.add(new BasicNameValuePair("13737", "116"));
		}
		if (regInfo.muslim) {
			applynvps.add(new BasicNameValuePair("13737", "3906"));
		}
		String type;
		String price;
		switch (regInfo.type) {
		case Full:
			type = "4126";
			price = "3500.00";
			break;
		case Student:
			type = "4136";
			price = "2500.00";
			break;
		case Guest:
			type = "4146";
			price = "1500.00";
			break;
		default:
			throw new RuntimeException("bad member type");
		}
		applynvps.add(new BasicNameValuePair("13917", type));
		applynvps.add(new BasicNameValuePair("amount", price));
		Send("Apply", applyUrl, applynvps);
	}

	private static String getIdUrl = "http://register.fudan.edu.cn/p/en/startApply/5308.html?projectId=5308&dataType=onlyRecord";
	private static Pattern idPattern = Pattern.compile("\"applyInfo\"\\:\\{\"id\"\\:\"(\\d+)\"");

	private String GetId() throws ClientProtocolException, IOException {
		String content = Send("GetId", getIdUrl, null);

		Matcher matcher = idPattern.matcher(content);
		if (!matcher.find()) {
			String message = "Id not found.";
			logger.error(message);
			throw new RuntimeException(message);
		}
		String id = matcher.group(1);
		logger.info("Get Id: " + id);
		return id;
	}

	private static String preparePayUrl = "http://register.fudan.edu.cn/p/en/preparePay.html?dataType=onlyRecord";
	private static Pattern payPasswordPattern = Pattern.compile("\"payPassword\":\"([^\"]+)\"");

	private String PreparePay(String id) throws ClientProtocolException, IOException {
		List<NameValuePair> nvps = Lists.newArrayList();
		nvps.add(new BasicNameValuePair("applyId", id));
		String content = Send("PreparePay", preparePayUrl, nvps);

		Matcher matcher = payPasswordPattern.matcher(content);
		if (!matcher.find()) {
			String message = "PayPassword not found.";
			logger.error(message);
			throw new RuntimeException(message);
		}
		String payPassword = matcher.group(1);
		logger.info("Get PayPassword: " + payPassword);
		return payPassword;
	}

	public String Do(String verify, RegInfo regInfo) throws ClientProtocolException, IOException {
		String logString = Objects.toStringHelper("params").add("verify", verify).add("regInfo", regInfo).toString();
		checkNotNull(regInfo);
		regInfo.check();
		logger.info(logString);
		String fullName = regInfo.firstName + " " + regInfo.lastName;
		Register(verify, fullName);
		Login();
		Apply(regInfo);
		String id = GetId();
		String payPassword = PreparePay(id);
		String url = "http://payment.fudan.edu.cn/pay_en/dealPay.html?pwd=" + payPassword;
		logger.info(url);
		httpclient.close();
		return url;
	}

	public static void main(String[] args) throws ClientProtocolException, IOException {
		FudanRegister fr = new FudanRegister();
		fr.GetV();
		System.out.println(fr.userId);
		Scanner scanner = new Scanner(System.in);
		String verify = scanner.next();
		scanner.close();
		RegInfo regInfo = new RegInfo();
		regInfo.paperId = "123";
		regInfo.firstName = "Chaohui";
		regInfo.lastName = "Wang";
		regInfo.country = "China";
		regInfo.affiliation = "Fudan University";
		regInfo.email = "chaohuiw@fudan.edu.cn";
		regInfo.type = MemberType.Student;
		regInfo.vegetarian = regInfo.muslim = true;
		fr.Do(verify, regInfo);
	}

}
